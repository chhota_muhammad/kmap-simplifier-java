/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package digitalcircuitsimplifier;

import javax.swing.JFrame;

/**
 *
 * @author chhota
 */
public class DrawingFrame extends JFrame{
    JFrame frame;
    String equ;
    public DrawingFrame(){
        frame= new JFrame("Digital Circuit Simplifier");
    }
    public void setEqu(String equ){
        this.equ=equ;
        frame.getContentPane().add(new DrawingPanel(equ));
        frame.setVisible(true);
          frame.setSize(1000, 750);
          
    }
}
