package digitalcircuitsimplifier;

import java.util.StringTokenizer;

public class SimulationLogic {
    char[] ch;int g;
    boolean[] b;
    String equ,desc;
    Gate root;int pr;
    StringTokenizer tok; boolean pos,finalop;
    boolean equop[];
    boolean not;
    public SimulationLogic(String equ,char[] c,boolean[] b,int p){
        this.equ=equ; this.ch=c; this.b=b; this.pr=p;
        root=new Gate(); pos=false; not=false;
        equop=new boolean[15];g=0;desc="";
    }
    public void makeGraph(){
        Gate temp;
        if(equ.charAt(0)=='('){
            tok=new StringTokenizer(equ,".");
            root.equ=".";
            pos=true;
        }
        else{
            tok=new StringTokenizer(equ,"+");
            root.equ="+";
        }
        while(tok.hasMoreTokens()){
            String abc=tok.nextToken();
            temp=new Gate();
            temp.equ=abc; 
            root.ip.add(temp);
        }
    }
    public void simulate(){
        boolean s;
        for(Gate value:root.ip){
            String temp=value.equ;
            if(temp.charAt(0)=='(')
                temp=temp.substring(1,temp.length()-1);
           boolean[] term=new boolean[15];
           int l=0;
           for(int i=temp.length()-1;i>=0;i--) {
             char c=temp.charAt(i);
             int n=(int)c;
             if(isAlphabate(c)){
                 //System.out.print(" "+c);
                 if(not==false)
                     term[l]=findboolean(c);
                 else{
                     term[l]=performNot(findboolean(c));
                     not=false;
                 }
                 l++;
             }
             if(n==39)
                 not=true;
           }
           for(int i=l-1;i>=0;i--)
               desc=desc+" "+convertBinary(term[i]);
           desc=desc+root.equ;
          
           if(root.equ.equals("+"))
                equop[g]=performAnd(term,l);
           else
               equop[g]=performOr(term,l);
           g++;
           }
         System.out.println(desc);
        for(int j=0;j<g;j++)
               System.out.print(" "+equop[j]);
        
           if(root.equ.equals("+"))
            finalop=performOr(equop,g);
            else
            finalop=performAnd(equop,g);
           System.out.println("Final Output"+finalop);
    }
  
    
    public boolean isAlphabate(char c){
       int bi=(int)c;
       if(bi<=122 && bi>=97 || bi>=65 && bi<=90)
            return true; 
       else
           return false;
   }
    
     public boolean findboolean(char c){
        for(int i=0;i<pr;i++){
            if(ch[i]==c)
                return b[i];
        }
        return false;  
    }
     
     public boolean performNot(boolean e){
        if(e==true)
            return false;
        else
            return true;   
    }
    
    public boolean performAnd(boolean[] term,int l){
        boolean qw=true;
        for(int i=0;i<l;i++){
            qw=qw && term[i];
        }
        
        return qw;   
    }
    
    public boolean performOr(boolean[] term,int l){
        boolean qw=false;
        for(int i=0;i<l;i++){
            qw=qw || term[i];
        }  
        return qw;   
    }
    public String finalop(){
        return convertBinary(finalop);
    }
    public String  convertBinary(boolean c){
        if(c==true)
            return "1";
        else
            return "0";
    }

    public String description() {
        desc=desc.substring(0, desc.length()-1);
        return desc;
    }
}
