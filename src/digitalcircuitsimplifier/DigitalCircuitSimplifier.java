
package digitalcircuitsimplifier;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

public class DigitalCircuitSimplifier extends JFrame{

    public DigitalCircuitSimplifier(){
        JFrame.setDefaultLookAndFeelDecorated(true);
         JDialog.setDefaultLookAndFeelDecorated(true);
        try
        {
           
            
               UIManager.setLookAndFeel("com.jtattoo.plaf.mcwin.McWinLookAndFeel");
  	}
  	catch (Exception ex)
  	{
   		System.out.println("Failed loading L&F: ");
   		System.out.println(ex);
  	}
        ImageIcon ic=new ImageIcon(this.getClass().getResource("Cir-Icon.png"));
        HomePage mf=new HomePage();
        mf.setVisible(true);
        mf.setTitle("Digital Circuit Simplifier");
        mf.setLocationRelativeTo(null);
        mf.setIconImage(ic.getImage());
        mf.setResizable(false);
        /*frame= new JFrame("Digital Circuit Simplifier");
        jp=new JTabbedPane();
        jp.add("Simplification",new EquationSimplifier());
        jp.add("K map",new Kmap());
        jp.add("Simulation",new Simulation());
        jp.add("Draw Circuit",new DrawCir());
        jp.add("Feedback",new Feedback());
        jp.add("Contact Us",new ContactUs());
        jp.add("FAQ",new FAQ());
        frame.getContentPane().add(jp);
         
        frame.setVisible(true);
          frame.setSize(750, 600);
          frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                **/
    }        
    public static void main(String[] args) {
        
            SwingUtilities.invokeLater(new Runnable()
            {
            public void run()
            {
                try {              
                    new DigitalCircuitSimplifier();
                } catch (Exception ex) {      }
            }
             });
             
        }
}
