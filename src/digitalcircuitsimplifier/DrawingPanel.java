
package digitalcircuitsimplifier;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class DrawingPanel extends JPanel{
 String  equ;
 boolean pos;
  Image andImage,orImage,notImage;
  StringTokenizer tok;
  int width,height,x,y;
  int pr;
  char ch[];
    public DrawingPanel(String equ){
      this.equ=equ;
      ch=new char[25];
      width=750;height=600;  x=600;y=150;
      pos=false;
     try {
         andImage=ImageIO.read(getClass().getResource("and1.png"));
         notImage=ImageIO.read(getClass().getResource("not1.png"));
         orImage=ImageIO.read(getClass().getResource("or1.png"));
     } catch (IOException ex) {
         Logger.getLogger(DrawingPanel.class.getName()).log(Level.SEVERE, null, ex);
     }    
    }
    
    protected void paintComponent(Graphics g)
    {
        Font f=new Font("Serif",Font.BOLD,18);
       g.setFont(f);
        g.drawString("Equation : "+equ, 15, 15);
        for(int i=0;i<equ.length();i++)
        {
            boolean present=false;
            char c=equ.charAt(i);
            int bi=(int)c;
            int p=0;
            if(bi<=122 && bi>=97 || bi>=65 && bi<=90)
            {
                for(int j=0;j<pr;j++)
                    if(c==ch[j])
                    {
                        present=true;
                        break;
                    }
                if(present==false)
                {
                    ch[pr]=c;
                    pr++;
                }
            }
        }
        x=75;y=75;
        int joint=8;
        for(int i=0;i<pr;i++){
            g.drawString(""+ch[i], x, y-30);
            g.drawLine(x, y, x, y+500);
            x+=75;
        }
        // Not line
        for(int i=equ.length()-1;i>0;i--)
        {
            
            char c=equ.charAt(i);
            int bi=(int)c;
            if(bi==39){
               int  p=findchar(equ.charAt(i-1));
               g.drawLine((p*75)+75, y+20, (p*75)+100+18, y+20);
               g.drawLine((p*75)+100+18, y+20,(p*75)+100+18,y+25);
               g.drawLine((p*75)+100+18, y+65, (p*75)+100+18, y+500);
               g.drawImage(notImage, (p*75)+100, y+25, null);
            }
        }
        int tokencount=sopPos();
        if(tokencount!=1){
        if(pos){
            g.drawImage(andImage,800 , height/2, null);
            g.drawLine(800+45, (height/2)+18, 800+45+20, (height/2)+18);
            g.drawString("Y", 800+45+20+10, (height/2)+18);
        }
        else{
            g.drawImage(orImage, 800, height/2, null);
            g.drawLine(800+45, (height/2)+18, 800+45+20, (height/2)+18);
            g.drawString("Y", 800+45+20+10, (height/2)+18);
        }
        x=600;y=150;int incY=((Integer)(400/tokencount));
        while(tok.hasMoreTokens()){
           boolean set=true; 
            String temp=tok.nextToken();
             if(temp.charAt(0)=='(')
                temp=temp.substring(1,temp.length()-1);
             temp=temp.trim();
             if(temp.length()<=2)
             if(temp.length()==2){
                 int bi=(int)temp.charAt(1);
                 if(bi<=122 && bi>=97 || bi>=65 && bi<=90)
                      set=true;
                 else{
                     drawforone(temp,g);
                     y+=incY;
                     set=false;
                 }
            }
             else{
                 drawforone(temp,g);
                 y+=incY;
                 set=false;
             }
            
            if(set)
            if(pos){
            g.drawImage(orImage,x, y, null); 
            g.drawLine(x+50, y+22, 800+4, (height/2)+joint);
            joinchar(temp,g);
            y+=incY;
            joint+=8;
            }
            else{
            g.drawImage(andImage, x, y, null);
            g.drawLine(x+45, y+18, 800, (height/2)+joint);
            joinchar(temp,g);
            y+=incY; joint+=8;
            }
        }
        }
        else
            drawforSingleInput(g);
    }
        public int findchar(char c){
            for(int i=0;i<pr;i++)
                if(ch[i]==c)
                    return i;
            return 0;
        }
        public int sopPos(){
        if(equ.charAt(0)=='('){
            pos=true;
            tok=new StringTokenizer(equ,".");
        }
        else
           tok=new StringTokenizer(equ,"+");
        int n=tok.countTokens();
        return n;
    }
    
    public void joinchar(String s,Graphics g){
        boolean not=false;
        int joint=8;
        for(int i=s.length()-1;i>=0;i--)
        { 
            
            char c=s.charAt(i);
            int bi=(int)c;
            if(bi<=122 && bi>=97 || bi>=65 && bi<=90 || bi==39){
            if(bi==39){
                not=true;
            }
            else{
               int  p=findchar(c); 
               if(not)
                   g.drawLine((p*75)+100+18, y+joint,x,y+joint);
                    
               else
                   g.drawLine((p*75)+75, y+joint, x, y+joint);
               not=false;
               joint+=8;
            }
            }
        }
    }
    
    public void drawforone(String s,Graphics g){
        
        int b=s.length();
        int  p=findchar(s.charAt(0)); 
        if(b==1){
            g.drawLine((p*75)+75, y,800, (height/2)+8);
          
        }
        else{
            g.drawLine((p*75)+100+18, y,800,(height/2)+8);
        }
    }
    
    public void drawforSingleInput(Graphics g){
        
        if(pos){
            g.drawImage(orImage, x, y, null);
            g.drawLine(x+45, y+18, x+45+20, y+18);
            g.drawString("Y", x+45+20+10, y+18);
        }
        else{
            g.drawImage(andImage,x , y, null);
            g.drawLine(x+45, y+18, x+45+20, y+18);
            g.drawString("Y",x+45+20+10, y+18);
        }
        joinchar(equ,g);
    }
    }
           

