package digitalcircuitsimplifier;
import java.util.ArrayList;
public class Gate {
    public ArrayList<Gate> ip;
    public int n;
    public Gate output;
    public String equ;
    public String rem;
    public Gate(){
        n=10;
        ip=new ArrayList<Gate>();
        output=null;
        equ="";
    }

    @Override
    public String toString() {
        return "Gate{" + "equ=" + equ + '}';
    }
    
    public Gate(String s) {
        n=10;
        ip=new ArrayList<Gate>();
        output=new Gate();   
        this.equ=s;
    }   
}